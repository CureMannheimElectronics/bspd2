EESchema Schematic File Version 4
LIBS:BSPD2_V2-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "BSPD2_V2"
Date "2018-08-14"
Rev "V1"
Comp "Cure Mannheim e.V."
Comment1 "Autor: Lukas Neumann"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L BSPD2_V2-rescue:R-78E5.0-0.5 U1
U 1 1 5B732A62
P 1350 1650
F 0 "U1" H 1900 1350 50  0000 L CNN
F 1 "R-78E5.0-0.5" H 1700 1850 50  0000 L CNN
F 2 "SHDRV3W100P0_254_1X3_1160X850X1040P" H 2400 1750 50  0001 L CNN
F 3 "http://docs-europe.electrocomponents.com/webdocs/10a2/0900766b810a2db7.pdf" H 2400 1650 50  0001 L CNN
F 4 "Switching Regulator,7-28Vin,5Vout 0.5A Switching Regulator, 7" H 2400 1550 50  0001 L CNN "Description"
F 5 "" H 2400 1450 50  0001 L CNN "Height"
F 6 "RECOM Power" H 2400 1350 50  0001 L CNN "Manufacturer_Name"
F 7 "R-78E5.0-0.5" H 2400 1250 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "1666675" H 2400 1150 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/1666675" H 2400 1050 50  0001 L CNN "RS Price/Stock"
F 10 "R-78E5.0-0.5" H 2400 950 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/r-78e5.0-0.5/recom-power" H 2400 850 50  0001 L CNN "Arrow Price/Stock"
	1    1350 1650
	1    0    0    -1  
$EndComp
$Comp
L BSPD2_V2-rescue:RT9193-33GB IC1
U 1 1 5B732B00
P 3750 1650
F 0 "IC1" H 4250 1800 50  0000 C CNN
F 1 "RT9193-33GB" H 4250 1300 50  0000 C CNN
F 2 "SOT94P279X130-5N" H 4250 1200 50  0001 C CNN
F 3 "http://www.richtek.com/assets/product_file/RT9193/DS9193-17.pdf" H 4250 1100 50  0001 C CNN
F 4 "300mA, Ultra-Low Noise, Ultra-Fast CMOS LDO Regulator" H 4250 1000 50  0001 C CNN "Description"
F 5 "RS" H 4250 900 50  0001 C CNN "Supplier_Name"
F 6 "" H 4250 800 50  0001 C CNN "RS Part Number"
F 7 "Richtek USA Inc." H 4250 700 50  0001 C CNN "Manufacturer_Name"
F 8 "RT9193-33GB" H 4250 600 50  0001 C CNN "Manufacturer_Part_Number"
F 9 "" H 4250 500 50  0001 C CNN "Allied_Number"
F 10 "" H 4250 400 50  0001 C CNN "Other Part Number"
F 11 "1.295" H 4600 300 50  0001 C CNN "Height"
	1    3750 1650
	1    0    0    -1  
$EndComp
$Comp
L BSPD2_V2-rescue:C0603C105Z8VACTU C1
U 1 1 5B732B87
P 1000 1950
F 0 "C1" H 1250 1800 50  0000 L CNN
F 1 "C0603C105Z8VACTU" H 1000 2150 50  0000 L CNN
F 2 "CAPC1608X90N" H 1350 2000 50  0001 L CNN
F 3 "https://search.kemet.com/component-edge/download/specsheet/C0603C105Z8VACTU.pdf" H 1350 1900 50  0001 L CNN
F 4 "KEMET - C0603C105Z8VACTU - CAP, MLCC, Y5V, 1UF, 10V, 0603" H 1350 1800 50  0001 L CNN "Description"
F 5 "0.9" H 1350 1700 50  0001 L CNN "Height"
F 6 "Kemet" H 1350 1600 50  0001 L CNN "Manufacturer_Name"
F 7 "C0603C105Z8VACTU" H 1350 1500 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 1350 1400 50  0001 L CNN "RS Part Number"
F 9 "" H 1350 1300 50  0001 L CNN "RS Price/Stock"
F 10 "70096976" H 1350 1200 50  0001 L CNN "Allied_Number"
F 11 "https://www.alliedelec.com/kemet-c0603c105z8vactu/70096976/" H 1350 1100 50  0001 L CNN "Allied Price/Stock"
F 12 "C0603C105Z8VACTU" H 1350 1000 50  0001 L CNN "Arrow Part Number"
F 13 "https://www.arrow.com/en/products/c0603c105z8vactu/kemet-corporation" H 1350 900 50  0001 L CNN "Arrow Price/Stock"
	1    1000 1950
	0    -1   -1   0   
$EndComp
$Comp
L power:+24V #PWR3
U 1 1 5B72D9A7
P 1000 1350
F 0 "#PWR3" H 1000 1200 50  0001 C CNN
F 1 "+24V" H 1000 1490 50  0000 C CNN
F 2 "" H 1000 1350 50  0001 C CNN
F 3 "" H 1000 1350 50  0001 C CNN
	1    1000 1350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR4
U 1 1 5B72D9C1
P 1000 2050
F 0 "#PWR4" H 1000 1800 50  0001 C CNN
F 1 "GND" H 1000 1900 50  0000 C CNN
F 2 "" H 1000 2050 50  0001 C CNN
F 3 "" H 1000 2050 50  0001 C CNN
	1    1000 2050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR7
U 1 1 5B72D9DB
P 2650 1650
F 0 "#PWR7" H 2650 1500 50  0001 C CNN
F 1 "+5V" H 2650 1790 50  0000 C CNN
F 2 "" H 2650 1650 50  0001 C CNN
F 3 "" H 2650 1650 50  0001 C CNN
	1    2650 1650
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR9
U 1 1 5B72DA66
P 3350 1500
F 0 "#PWR9" H 3350 1350 50  0001 C CNN
F 1 "+5V" H 3350 1640 50  0000 C CNN
F 2 "" H 3350 1500 50  0001 C CNN
F 3 "" H 3350 1500 50  0001 C CNN
	1    3350 1500
	1    0    0    -1  
$EndComp
$Comp
L BSPD2_V2-rescue:C0603C105Z8VACTU C2
U 1 1 5B72DA84
P 3350 2100
F 0 "C2" H 3600 1950 50  0000 L CNN
F 1 "C0603C105Z8VACTU" H 3350 2300 50  0000 L CNN
F 2 "CAPC1608X90N" H 3700 2150 50  0001 L CNN
F 3 "https://search.kemet.com/component-edge/download/specsheet/C0603C105Z8VACTU.pdf" H 3700 2050 50  0001 L CNN
F 4 "KEMET - C0603C105Z8VACTU - CAP, MLCC, Y5V, 1UF, 10V, 0603" H 3700 1950 50  0001 L CNN "Description"
F 5 "0.9" H 3700 1850 50  0001 L CNN "Height"
F 6 "Kemet" H 3700 1750 50  0001 L CNN "Manufacturer_Name"
F 7 "C0603C105Z8VACTU" H 3700 1650 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 3700 1550 50  0001 L CNN "RS Part Number"
F 9 "" H 3700 1450 50  0001 L CNN "RS Price/Stock"
F 10 "70096976" H 3700 1350 50  0001 L CNN "Allied_Number"
F 11 "https://www.alliedelec.com/kemet-c0603c105z8vactu/70096976/" H 3700 1250 50  0001 L CNN "Allied Price/Stock"
F 12 "C0603C105Z8VACTU" H 3700 1150 50  0001 L CNN "Arrow Part Number"
F 13 "https://www.arrow.com/en/products/c0603c105z8vactu/kemet-corporation" H 3700 1050 50  0001 L CNN "Arrow Price/Stock"
	1    3350 2100
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR10
U 1 1 5B72DACE
P 3350 2200
F 0 "#PWR10" H 3350 1950 50  0001 C CNN
F 1 "GND" H 3350 2050 50  0000 C CNN
F 2 "" H 3350 2200 50  0001 C CNN
F 3 "" H 3350 2200 50  0001 C CNN
	1    3350 2200
	1    0    0    -1  
$EndComp
$Comp
L BSPD2_V2-rescue:C0603C105Z8VACTU C3
U 1 1 5B72DAEF
P 5000 2300
F 0 "C3" H 5200 2150 50  0000 L CNN
F 1 "C0603C105Z8VACTU" H 4750 2500 50  0000 L CNN
F 2 "CAPC1608X90N" H 5350 2350 50  0001 L CNN
F 3 "https://search.kemet.com/component-edge/download/specsheet/C0603C105Z8VACTU.pdf" H 5350 2250 50  0001 L CNN
F 4 "KEMET - C0603C105Z8VACTU - CAP, MLCC, Y5V, 1UF, 10V, 0603" H 5350 2150 50  0001 L CNN "Description"
F 5 "0.9" H 5350 2050 50  0001 L CNN "Height"
F 6 "Kemet" H 5350 1950 50  0001 L CNN "Manufacturer_Name"
F 7 "C0603C105Z8VACTU" H 5350 1850 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 5350 1750 50  0001 L CNN "RS Part Number"
F 9 "" H 5350 1650 50  0001 L CNN "RS Price/Stock"
F 10 "70096976" H 5350 1550 50  0001 L CNN "Allied_Number"
F 11 "https://www.alliedelec.com/kemet-c0603c105z8vactu/70096976/" H 5350 1450 50  0001 L CNN "Allied Price/Stock"
F 12 "C0603C105Z8VACTU" H 5350 1350 50  0001 L CNN "Arrow Part Number"
F 13 "https://www.arrow.com/en/products/c0603c105z8vactu/kemet-corporation" H 5350 1250 50  0001 L CNN "Arrow Price/Stock"
	1    5000 2300
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR13
U 1 1 5B72DB76
P 5000 1450
F 0 "#PWR13" H 5000 1300 50  0001 C CNN
F 1 "+3.3V" H 5000 1590 50  0000 C CNN
F 2 "" H 5000 1450 50  0001 C CNN
F 3 "" H 5000 1450 50  0001 C CNN
	1    5000 1450
	1    0    0    -1  
$EndComp
$Comp
L BSPD2_V2-rescue:C0603C223K5RACTU C4
U 1 1 5B72DB94
P 5450 2150
F 0 "C4" H 5650 2000 50  0000 L CNN
F 1 "C0603C223K5RACTU" H 5200 2300 50  0000 L CNN
F 2 "CAPC1608X87N" H 5800 2200 50  0001 L CNN
F 3 "http://docs-europe.electrocomponents.com/webdocs/134c/0900766b8134ce68.pdf" H 5800 2100 50  0001 L CNN
F 4 "0603 X7R ceramic capacitor, 50V 22nF Kemet 0603 22nF Ceramic Multilayer Capacitor, 50V dc, 125C, X7R Dielectric, 10% SMD" H 5800 2000 50  0001 L CNN "Description"
F 5 "0.87" H 5800 1900 50  0001 L CNN "Height"
F 6 "Kemet" H 5800 1800 50  0001 L CNN "Manufacturer_Name"
F 7 "C0603C223K5RACTU" H 5800 1700 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "2644602P" H 5800 1600 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/2644602P" H 5800 1500 50  0001 L CNN "RS Price/Stock"
F 10 "70096988" H 5800 1400 50  0001 L CNN "Allied_Number"
F 11 "https://www.alliedelec.com/kemet-c0603c223k5ractu/70096988/" H 5800 1300 50  0001 L CNN "Allied Price/Stock"
F 12 "C0603C223K5RACTU" H 5800 1200 50  0001 L CNN "Arrow Part Number"
F 13 "https://www.arrow.com/en/products/c0603c223k5ractu/kemet-corporation" H 5800 1100 50  0001 L CNN "Arrow Price/Stock"
	1    5450 2150
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR14
U 1 1 5B72DCDB
P 5000 2500
F 0 "#PWR14" H 5000 2250 50  0001 C CNN
F 1 "GND" H 5000 2350 50  0000 C CNN
F 2 "" H 5000 2500 50  0001 C CNN
F 3 "" H 5000 2500 50  0001 C CNN
	1    5000 2500
	1    0    0    -1  
$EndComp
$Comp
L BSPD2_V2-rescue:LM393AD IC2
U 1 1 5B72DF1D
P 4100 3600
F 0 "IC2" H 4600 3150 50  0000 L CNN
F 1 "LM393AD" H 4550 3750 50  0000 L CNN
F 2 "SOIC127P600X175-8N" H 5050 3700 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393a.pdf" H 5050 3600 50  0001 L CNN
F 4 "LM393ADG4, Dual Comparator Open Collector 1.3s 2-kanalers 3, 5, 9, 12, 15, 18, 24, 28V 8-Pin SOIC" H 5050 3500 50  0001 L CNN "Description"
F 5 "1.75" H 5050 3400 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 5050 3300 50  0001 L CNN "Manufacturer_Name"
F 7 "LM393AD" H 5050 3200 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "6612810P" H 5050 3100 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/6612810P" H 5050 3000 50  0001 L CNN "RS Price/Stock"
F 10 "LM393AD" H 5050 2900 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/lm393ad/texas-instruments" H 5050 2800 50  0001 L CNN "Arrow Price/Stock"
	1    4100 3600
	1    0    0    -1  
$EndComp
Text GLabel 4100 3700 0    51   Input ~ 0
Sensorvoltage
Text GLabel 4100 3600 0    51   Input ~ 0
Sensoractive(inverted)
Text Notes 7050 6300 0    118  ~ 0
Turn-on delay is missing.
$Comp
L power:GND #PWR12
U 1 1 5B72E2BC
P 4100 4200
F 0 "#PWR12" H 4100 3950 50  0001 C CNN
F 1 "GND" H 4100 4050 50  0000 C CNN
F 2 "" H 4100 4200 50  0001 C CNN
F 3 "" H 4100 4200 50  0001 C CNN
	1    4100 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5B72E2FF
P 3900 4000
F 0 "R6" V 3980 4000 50  0000 C CNN
F 1 "R" V 3900 4000 50  0000 C CNN
F 2 "" V 3830 4000 50  0001 C CNN
F 3 "" H 3900 4000 50  0001 C CNN
	1    3900 4000
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR17
U 1 1 5B72E4C4
P 5250 3300
F 0 "#PWR17" H 5250 3150 50  0001 C CNN
F 1 "+5V" H 5250 3440 50  0000 C CNN
F 2 "" H 5250 3300 50  0001 C CNN
F 3 "" H 5250 3300 50  0001 C CNN
	1    5250 3300
	1    0    0    -1  
$EndComp
$Comp
L BSPD2_V2-rescue:SML-D13FWT86 LED1
U 1 1 5B72E518
P 900 4800
F 0 "LED1" H 1150 4600 50  0000 L BNN
F 1 "SML-D13FWT86" H 950 5050 50  0000 L BNN
F 2 "SMLD15YWT86" H 1400 4950 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 1400 4850 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 1400 4750 50  0001 L BNN "Description"
F 5 "" H 1400 4650 50  0001 L BNN "Height"
F 6 "ROHM Semiconductor" H 1400 4550 50  0001 L BNN "Manufacturer_Name"
F 7 "SML-D13FWT86" H 1400 4450 50  0001 L BNN "Manufacturer_Part_Number"
F 8 "1332874P" H 1400 4350 50  0001 L BNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/1332874P" H 1400 4250 50  0001 L BNN "RS Price/Stock"
F 10 "SML-D13FWT86" H 1400 4150 50  0001 L BNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/1332874P" H 1400 4050 50  0001 L BNN "Arrow Price/Stock"
	1    900  4800
	0    -1   -1   0   
$EndComp
$Comp
L BSPD2_V2-rescue:SML-D13DWT86A LED2
U 1 1 5B72E76A
P 1550 4800
F 0 "LED2" H 1800 4600 50  0000 L BNN
F 1 "SML-D13DWT86A" H 1550 5050 50  0000 L BNN
F 2 "LEDC1608X65N" H 2050 4950 50  0001 L BNN
F 3 "https://docs-emea.rs-online.com/webdocs/156b/0900766b8156b2c9.pdf" H 2050 4850 50  0001 L BNN
F 4 "ROHM SML-D13DWT86A, SML 608 nm Orange LED, 1608 (0603) Milky White SMD package" H 2050 4750 50  0001 L BNN "Description"
F 5 "0.65" H 2050 4650 50  0001 L BNN "Height"
F 6 "ROHM Semiconductor" H 2050 4550 50  0001 L BNN "Manufacturer_Name"
F 7 "SML-D13DWT86A" H 2050 4450 50  0001 L BNN "Manufacturer_Part_Number"
F 8 "1332873P" H 2050 4350 50  0001 L BNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/1332873P" H 2050 4250 50  0001 L BNN "RS Price/Stock"
F 10 "SML-D13DWT86A" H 2050 4150 50  0001 L BNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/1332873P" H 2050 4050 50  0001 L BNN "Arrow Price/Stock"
	1    1550 4800
	0    -1   -1   0   
$EndComp
$Comp
L BSPD2_V2-rescue:RC0603JR-0768RL R1
U 1 1 5B72E8A9
P 900 4900
F 0 "R1" H 1200 5000 50  0000 L CNN
F 1 "RC0603JR-0768RL" H 900 4650 50  0000 L CNN
F 2 "RESC1608X55N" H 1450 4950 50  0001 L CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC0603_51_RoHS_L_v5.pdf" H 1450 4850 50  0001 L CNN
F 4 "RESISTOR, 0603 68 Ohms 5% 1/10 W" H 1450 4750 50  0001 L CNN "Description"
F 5 "0.55" H 1450 4650 50  0001 L CNN "Height"
F 6 "YAGEO (PHYCOMP)" H 1450 4550 50  0001 L CNN "Manufacturer_Name"
F 7 "RC0603JR-0768RL" H 1450 4450 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 1450 4350 50  0001 L CNN "RS Part Number"
F 9 "" H 1450 4250 50  0001 L CNN "RS Price/Stock"
F 10 "RC0603JR-0768RL" H 1450 4150 50  0001 L CNN "Arrow Part Number"
F 11 "" H 1450 4050 50  0001 L CNN "Arrow Price/Stock"
	1    900  4900
	0    1    1    0   
$EndComp
$Comp
L BSPD2_V2-rescue:RC0603JR-0768RL R2
U 1 1 5B72E970
P 1550 4900
F 0 "R2" H 1850 5000 50  0000 L CNN
F 1 "RC0603JR-0768RL" H 1550 4700 50  0000 L CNN
F 2 "RESC1608X55N" H 2100 4950 50  0001 L CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC0603_51_RoHS_L_v5.pdf" H 2100 4850 50  0001 L CNN
F 4 "RESISTOR, 0603 68 Ohms 5% 1/10 W" H 2100 4750 50  0001 L CNN "Description"
F 5 "0.55" H 2100 4650 50  0001 L CNN "Height"
F 6 "YAGEO (PHYCOMP)" H 2100 4550 50  0001 L CNN "Manufacturer_Name"
F 7 "RC0603JR-0768RL" H 2100 4450 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 2100 4350 50  0001 L CNN "RS Part Number"
F 9 "" H 2100 4250 50  0001 L CNN "RS Price/Stock"
F 10 "RC0603JR-0768RL" H 2100 4150 50  0001 L CNN "Arrow Part Number"
F 11 "" H 2100 4050 50  0001 L CNN "Arrow Price/Stock"
	1    1550 4900
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR2
U 1 1 5B72EB11
P 900 5650
F 0 "#PWR2" H 900 5400 50  0001 C CNN
F 1 "GND" H 900 5500 50  0000 C CNN
F 2 "" H 900 5650 50  0001 C CNN
F 3 "" H 900 5650 50  0001 C CNN
	1    900  5650
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR5
U 1 1 5B72EC23
P 1550 4150
F 0 "#PWR5" H 1550 4000 50  0001 C CNN
F 1 "+3.3V" H 1550 4290 50  0000 C CNN
F 2 "" H 1550 4150 50  0001 C CNN
F 3 "" H 1550 4150 50  0001 C CNN
	1    1550 4150
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR1
U 1 1 5B72EC52
P 900 4150
F 0 "#PWR1" H 900 4000 50  0001 C CNN
F 1 "+3.3V" H 900 4290 50  0000 C CNN
F 2 "" H 900 4150 50  0001 C CNN
F 3 "" H 900 4150 50  0001 C CNN
	1    900  4150
	1    0    0    -1  
$EndComp
$Comp
L BSPD2_V2-rescue:BC847,235 Q1
U 1 1 5B72F1D7
P 5150 6400
F 0 "Q1" H 5600 6450 50  0000 L CNN
F 1 "BC847,235" H 5600 6350 50  0000 L CNN
F 2 "SOT95P230X110-3N" H 5600 6250 50  0001 L CNN
F 3 "http://assets.nexperia.com/documents/data-sheet/BC847_SER.pdf" H 5600 6150 50  0001 L CNN
F 4 "BC847 series - 45 V, 100 mA NPN general-purpose transistors" H 5600 6050 50  0001 L CNN "Description"
F 5 "1.1" H 5600 5950 50  0001 L CNN "Height"
F 6 "Nexperia" H 5600 5850 50  0001 L CNN "Manufacturer_Name"
F 7 "BC847,235" H 5600 5750 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 5600 5650 50  0001 L CNN "RS Part Number"
F 9 "" H 5600 5550 50  0001 L CNN "RS Price/Stock"
F 10 "BC847,235" H 5600 5450 50  0001 L CNN "Arrow Part Number"
F 11 "http://www.arrow.com/en/products/bc847235/nexperia" H 5600 5350 50  0001 L CNN "Arrow Price/Stock"
	1    5150 6400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR15
U 1 1 5B72F2B2
P 5050 7400
F 0 "#PWR15" H 5050 7150 50  0001 C CNN
F 1 "GND" H 5050 7250 50  0000 C CNN
F 2 "" H 5050 7400 50  0001 C CNN
F 3 "" H 5050 7400 50  0001 C CNN
	1    5050 7400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5B72F31C
P 3700 3800
F 0 "R5" V 3780 3800 50  0000 C CNN
F 1 "R" V 3700 3800 50  0000 C CNN
F 2 "" V 3630 3800 50  0001 C CNN
F 3 "" H 3700 3800 50  0001 C CNN
	1    3700 3800
	0    1    1    0   
$EndComp
$Comp
L BSPD2_V2-rescue:RC0603JR-0710KL R7
U 1 1 5B72F3DD
P 5050 7200
F 0 "R7" H 5400 7300 50  0000 C CNN
F 1 "RC0603JR-0710KL" H 5400 7100 50  0000 C CNN
F 2 "RESC1608X55N" H 5400 7000 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC0603_51_RoHS_L_v5.pdf" H 5400 6900 50  0001 C CNN
F 4 "YAGEO (PHYCOMP) - RC0603JR-0710KL - RES, THICK FILM, 10K, 5%, 0.1W, 0603" H 5400 6800 50  0001 C CNN "Description"
F 5 "RS" H 5400 6700 50  0001 C CNN "Supplier_Name"
F 6 "" H 5400 6600 50  0001 C CNN "RS Part Number"
F 7 "YAGEO (PHYCOMP)" H 5400 6500 50  0001 C CNN "Manufacturer_Name"
F 8 "RC0603JR-0710KL" H 5400 6400 50  0001 C CNN "Manufacturer_Part_Number"
F 9 "" H 5400 6300 50  0001 C CNN "Allied_Number"
F 10 "" H 5400 6200 50  0001 C CNN "Other Part Number"
F 11 "0.55" H 5400 6100 50  0001 C CNN "Height"
	1    5050 7200
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR16
U 1 1 5B72F706
P 5450 6050
F 0 "#PWR16" H 5450 5900 50  0001 C CNN
F 1 "+5V" H 5450 6190 50  0000 C CNN
F 2 "" H 5450 6050 50  0001 C CNN
F 3 "" H 5450 6050 50  0001 C CNN
	1    5450 6050
	1    0    0    -1  
$EndComp
Text Notes 2600 850  0    118  ~ 0
Power Supply
Wire Wire Line
	1000 1350 1000 1400
Wire Wire Line
	1000 1950 1000 2000
Wire Wire Line
	1350 1750 1200 1750
Wire Wire Line
	1200 1750 1200 2000
Wire Wire Line
	1200 2000 1000 2000
Connection ~ 1000 2000
Wire Wire Line
	1350 1650 1200 1650
Wire Wire Line
	1200 1650 1200 1400
Wire Wire Line
	1200 1400 1000 1400
Connection ~ 1000 1400
Wire Wire Line
	2550 1650 2650 1650
Wire Wire Line
	3350 1500 3350 1550
Wire Wire Line
	3350 2100 3350 2150
Wire Wire Line
	3550 1650 3750 1650
Wire Wire Line
	3550 1550 3550 1650
Wire Wire Line
	3550 1550 3350 1550
Connection ~ 3350 1550
Wire Wire Line
	3550 1850 3750 1850
Connection ~ 3550 1650
Wire Wire Line
	3750 1750 3700 1750
Wire Wire Line
	3700 1750 3700 2150
Wire Wire Line
	3700 2150 3350 2150
Connection ~ 3350 2150
Wire Wire Line
	4750 1650 5450 1650
Wire Wire Line
	5450 2150 5450 2450
Wire Wire Line
	5450 2450 5000 2450
Wire Wire Line
	5000 2300 5000 2450
Connection ~ 5000 2450
Wire Wire Line
	5000 1450 5000 1750
Wire Wire Line
	4750 1750 5000 1750
Connection ~ 5000 1750
Wire Wire Line
	4100 3900 4100 4150
Wire Wire Line
	3850 3800 3900 3800
Wire Wire Line
	3900 3850 3900 3800
Connection ~ 3900 3800
Wire Wire Line
	3900 4150 4100 4150
Connection ~ 4100 4150
Wire Wire Line
	900  5600 900  5650
Wire Wire Line
	1550 5600 1550 5650
Wire Wire Line
	900  4800 900  4900
Wire Wire Line
	1550 4800 1550 4900
Wire Wire Line
	900  4150 900  4200
Wire Wire Line
	1550 4200 1550 4150
Wire Wire Line
	3500 3800 3550 3800
Wire Wire Line
	4950 6400 5050 6400
Wire Wire Line
	5050 6400 5050 6500
Connection ~ 5050 6400
Wire Wire Line
	5050 7200 5050 7400
Wire Wire Line
	5450 6700 5450 6850
Wire Wire Line
	5450 6050 5450 6100
Wire Notes Line
	3150 2900 3150 7650
Wire Notes Line
	3250 7650 3300 7650
Wire Notes Line
	6950 6150 6950 600 
Wire Notes Line
	3150 5250 6850 5250
Text Notes 1550 3400 0    118  ~ 0
LEDs
Text Notes 4400 3050 0    118  ~ 0
Comparator
Text Notes 4150 5600 0    118  ~ 0
Output(Sensoractive)
Text Notes 8550 800  0    118  ~ 0
Connectors
Text GLabel 4950 6400 0    51   Input ~ 0
Sensoractive(inverted)
$Comp
L power:+5V #PWR11
U 1 1 5B72E976
P 3500 3800
F 0 "#PWR11" H 3500 3650 50  0001 C CNN
F 1 "+5V" H 3500 3940 50  0000 C CNN
F 2 "" H 3500 3800 50  0001 C CNN
F 3 "" H 3500 3800 50  0001 C CNN
	1    3500 3800
	0    -1   -1   0   
$EndComp
$Comp
L BSPD2_V2-rescue:NC7SZ32M5X IC3
U 1 1 5B72E9C0
P 8750 2950
F 0 "IC3" H 9200 2550 50  0000 L CNN
F 1 "NC7SZ32M5X" H 9050 3150 50  0000 L CNN
F 2 "SOT95P280X145-5N" H 9600 3050 50  0001 L CNN
F 3 "http://docs-europe.electrocomponents.com/webdocs/0f9b/0900766b80f9b555.pdf" H 9600 2950 50  0001 L CNN
F 4 "OR Gate UHS 2 Input CMOS SOT23 5 Pin" H 9600 2850 50  0001 L CNN "Description"
F 5 "1.45" H 9600 2750 50  0001 L CNN "Height"
F 6 "Fairchild Semiconductor" H 9600 2650 50  0001 L CNN "Manufacturer_Name"
F 7 "NC7SZ32M5X" H 9600 2550 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "1698554" H 9600 2450 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/1698554" H 9600 2350 50  0001 L CNN "RS Price/Stock"
F 10 "NC7SZ32M5X" H 9600 2250 50  0001 L CNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/1698554" H 9600 2150 50  0001 L CNN "Arrow Price/Stock"
	1    8750 2950
	1    0    0    -1  
$EndComp
$Comp
L BSPD2_V2-rescue:BC847,235 Q2
U 1 1 5B72EBE0
P 9250 5100
F 0 "Q2" H 9700 5150 50  0000 L CNN
F 1 "BC847,235" H 9700 5050 50  0000 L CNN
F 2 "SOT95P230X110-3N" H 9700 4950 50  0001 L CNN
F 3 "http://assets.nexperia.com/documents/data-sheet/BC847_SER.pdf" H 9700 4850 50  0001 L CNN
F 4 "BC847 series - 45 V, 100 mA NPN general-purpose transistors" H 9700 4750 50  0001 L CNN "Description"
F 5 "1.1" H 9700 4650 50  0001 L CNN "Height"
F 6 "Nexperia" H 9700 4550 50  0001 L CNN "Manufacturer_Name"
F 7 "BC847,235" H 9700 4450 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 9700 4350 50  0001 L CNN "RS Part Number"
F 9 "" H 9700 4250 50  0001 L CNN "RS Price/Stock"
F 10 "BC847,235" H 9700 4150 50  0001 L CNN "Arrow Part Number"
F 11 "http://www.arrow.com/en/products/bc847235/nexperia" H 9700 4050 50  0001 L CNN "Arrow Price/Stock"
	1    9250 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR24
U 1 1 5B72EBE6
P 9150 5950
F 0 "#PWR24" H 9150 5700 50  0001 C CNN
F 1 "GND" H 9150 5800 50  0000 C CNN
F 2 "" H 9150 5950 50  0001 C CNN
F 3 "" H 9150 5950 50  0001 C CNN
	1    9150 5950
	1    0    0    -1  
$EndComp
$Comp
L BSPD2_V2-rescue:RC0603JR-0710KL R10
U 1 1 5B72EBF4
P 9150 5900
F 0 "R10" H 9500 6000 50  0000 C CNN
F 1 "RC0603JR-0710KL" H 9500 5800 50  0000 C CNN
F 2 "RESC1608X55N" H 9500 5700 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC0603_51_RoHS_L_v5.pdf" H 9500 5600 50  0001 C CNN
F 4 "YAGEO (PHYCOMP) - RC0603JR-0710KL - RES, THICK FILM, 10K, 5%, 0.1W, 0603" H 9500 5500 50  0001 C CNN "Description"
F 5 "RS" H 9500 5400 50  0001 C CNN "Supplier_Name"
F 6 "" H 9500 5300 50  0001 C CNN "RS Part Number"
F 7 "YAGEO (PHYCOMP)" H 9500 5200 50  0001 C CNN "Manufacturer_Name"
F 8 "RC0603JR-0710KL" H 9500 5100 50  0001 C CNN "Manufacturer_Part_Number"
F 9 "" H 9500 5000 50  0001 C CNN "Allied_Number"
F 10 "" H 9500 4900 50  0001 C CNN "Other Part Number"
F 11 "0.55" H 9500 4800 50  0001 C CNN "Height"
	1    9150 5900
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR25
U 1 1 5B72EC00
P 9550 4750
F 0 "#PWR25" H 9550 4600 50  0001 C CNN
F 1 "+5V" H 9550 4890 50  0000 C CNN
F 2 "" H 9550 4750 50  0001 C CNN
F 3 "" H 9550 4750 50  0001 C CNN
	1    9550 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 5100 9150 5100
Wire Wire Line
	9150 5100 9150 5200
Connection ~ 9150 5100
Wire Wire Line
	9150 5900 9150 5950
Wire Wire Line
	9550 5400 9550 5450
Wire Wire Line
	9550 4750 9550 4800
Text GLabel 5450 6850 3    51   Input ~ 0
Output(SA(inverted))
Text GLabel 9750 2950 2    51   Input ~ 0
SA(inv)&BA(inv)
$Comp
L power:+5V #PWR27
U 1 1 5B72EDEC
P 10350 3050
F 0 "#PWR27" H 10350 2900 50  0001 C CNN
F 1 "+5V" H 10350 3190 50  0000 C CNN
F 2 "" H 10350 3050 50  0001 C CNN
F 3 "" H 10350 3050 50  0001 C CNN
	1    10350 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	9750 3050 10000 3050
$Comp
L power:GND #PWR23
U 1 1 5B72EE9D
P 8750 3300
F 0 "#PWR23" H 8750 3050 50  0001 C CNN
F 1 "GND" H 8750 3150 50  0000 C CNN
F 2 "" H 8750 3300 50  0001 C CNN
F 3 "" H 8750 3300 50  0001 C CNN
	1    8750 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 3150 8750 3300
$Comp
L BSPD2_V2-rescue:C0603C105Z8VACTU C5
U 1 1 5B72EF50
P 5450 3350
F 0 "C5" H 5700 3200 50  0000 L CNN
F 1 "C0603C105Z8VACTU" H 5450 3550 50  0000 L CNN
F 2 "CAPC1608X90N" H 5800 3400 50  0001 L CNN
F 3 "https://search.kemet.com/component-edge/download/specsheet/C0603C105Z8VACTU.pdf" H 5800 3300 50  0001 L CNN
F 4 "KEMET - C0603C105Z8VACTU - CAP, MLCC, Y5V, 1UF, 10V, 0603" H 5800 3200 50  0001 L CNN "Description"
F 5 "0.9" H 5800 3100 50  0001 L CNN "Height"
F 6 "Kemet" H 5800 3000 50  0001 L CNN "Manufacturer_Name"
F 7 "C0603C105Z8VACTU" H 5800 2900 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 5800 2800 50  0001 L CNN "RS Part Number"
F 9 "" H 5800 2700 50  0001 L CNN "RS Price/Stock"
F 10 "70096976" H 5800 2600 50  0001 L CNN "Allied_Number"
F 11 "https://www.alliedelec.com/kemet-c0603c105z8vactu/70096976/" H 5800 2500 50  0001 L CNN "Allied Price/Stock"
F 12 "C0603C105Z8VACTU" H 5800 2400 50  0001 L CNN "Arrow Part Number"
F 13 "https://www.arrow.com/en/products/c0603c105z8vactu/kemet-corporation" H 5800 2300 50  0001 L CNN "Arrow Price/Stock"
	1    5450 3350
	1    0    0    -1  
$EndComp
$Comp
L BSPD2_V2-rescue:C0603C105Z8VACTU C6
U 1 1 5B72F027
P 10000 3650
F 0 "C6" H 10250 3500 50  0000 L CNN
F 1 "C0603C105Z8VACTU" H 9800 3850 50  0000 L CNN
F 2 "CAPC1608X90N" H 10350 3700 50  0001 L CNN
F 3 "https://search.kemet.com/component-edge/download/specsheet/C0603C105Z8VACTU.pdf" H 10350 3600 50  0001 L CNN
F 4 "KEMET - C0603C105Z8VACTU - CAP, MLCC, Y5V, 1UF, 10V, 0603" H 10350 3500 50  0001 L CNN "Description"
F 5 "0.9" H 10350 3400 50  0001 L CNN "Height"
F 6 "Kemet" H 10350 3300 50  0001 L CNN "Manufacturer_Name"
F 7 "C0603C105Z8VACTU" H 10350 3200 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 10350 3100 50  0001 L CNN "RS Part Number"
F 9 "" H 10350 3000 50  0001 L CNN "RS Price/Stock"
F 10 "70096976" H 10350 2900 50  0001 L CNN "Allied_Number"
F 11 "https://www.alliedelec.com/kemet-c0603c105z8vactu/70096976/" H 10350 2800 50  0001 L CNN "Allied Price/Stock"
F 12 "C0603C105Z8VACTU" H 10350 2700 50  0001 L CNN "Arrow Part Number"
F 13 "https://www.arrow.com/en/products/c0603c105z8vactu/kemet-corporation" H 10350 2600 50  0001 L CNN "Arrow Price/Stock"
	1    10000 3650
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR26
U 1 1 5B72F119
P 10000 3750
F 0 "#PWR26" H 10000 3500 50  0001 C CNN
F 1 "GND" H 10000 3600 50  0000 C CNN
F 2 "" H 10000 3750 50  0001 C CNN
F 3 "" H 10000 3750 50  0001 C CNN
	1    10000 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 3650 10000 3750
Wire Wire Line
	10000 3150 10000 3050
Connection ~ 10000 3050
$Comp
L power:GND #PWR18
U 1 1 5B72F210
P 6050 3350
F 0 "#PWR18" H 6050 3100 50  0001 C CNN
F 1 "GND" H 6050 3200 50  0000 C CNN
F 2 "" H 6050 3350 50  0001 C CNN
F 3 "" H 6050 3350 50  0001 C CNN
	1    6050 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 3350 5950 3350
Wire Wire Line
	5200 3600 5250 3600
Text GLabel 9550 5450 3    51   Input ~ 0
Output(BSPD2)
Text GLabel 9050 5100 0    51   Input ~ 0
SA(inv)&BA(inv)
Text GLabel 8750 1600 0    51   Input ~ 0
Output(BSPD2)
Text GLabel 8750 1450 0    51   Input ~ 0
Output(BSPD1)
$Comp
L BSPD2_V2-rescue:SML-D13DWT86A LED3
U 1 1 5B72F8E4
P 2250 4800
F 0 "LED3" H 2500 4600 50  0000 L BNN
F 1 "SML-D13DWT86A" H 2250 5050 50  0000 L BNN
F 2 "LEDC1608X65N" H 2750 4950 50  0001 L BNN
F 3 "https://docs-emea.rs-online.com/webdocs/156b/0900766b8156b2c9.pdf" H 2750 4850 50  0001 L BNN
F 4 "ROHM SML-D13DWT86A, SML 608 nm Orange LED, 1608 (0603) Milky White SMD package" H 2750 4750 50  0001 L BNN "Description"
F 5 "0.65" H 2750 4650 50  0001 L BNN "Height"
F 6 "ROHM Semiconductor" H 2750 4550 50  0001 L BNN "Manufacturer_Name"
F 7 "SML-D13DWT86A" H 2750 4450 50  0001 L BNN "Manufacturer_Part_Number"
F 8 "1332873P" H 2750 4350 50  0001 L BNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/1332873P" H 2750 4250 50  0001 L BNN "RS Price/Stock"
F 10 "SML-D13DWT86A" H 2750 4150 50  0001 L BNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/1332873P" H 2750 4050 50  0001 L BNN "Arrow Price/Stock"
	1    2250 4800
	0    -1   -1   0   
$EndComp
$Comp
L BSPD2_V2-rescue:RC0603JR-0768RL R3
U 1 1 5B72F8F2
P 2250 4900
F 0 "R3" H 2550 5000 50  0000 L CNN
F 1 "RC0603JR-0768RL" H 2250 4700 50  0000 L CNN
F 2 "RESC1608X55N" H 2800 4950 50  0001 L CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC0603_51_RoHS_L_v5.pdf" H 2800 4850 50  0001 L CNN
F 4 "RESISTOR, 0603 68 Ohms 5% 1/10 W" H 2800 4750 50  0001 L CNN "Description"
F 5 "0.55" H 2800 4650 50  0001 L CNN "Height"
F 6 "YAGEO (PHYCOMP)" H 2800 4550 50  0001 L CNN "Manufacturer_Name"
F 7 "RC0603JR-0768RL" H 2800 4450 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 2800 4350 50  0001 L CNN "RS Part Number"
F 9 "" H 2800 4250 50  0001 L CNN "RS Price/Stock"
F 10 "RC0603JR-0768RL" H 2800 4150 50  0001 L CNN "Arrow Part Number"
F 11 "" H 2800 4050 50  0001 L CNN "Arrow Price/Stock"
	1    2250 4900
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR6
U 1 1 5B72F8F8
P 2250 4150
F 0 "#PWR6" H 2250 4000 50  0001 C CNN
F 1 "+3.3V" H 2250 4290 50  0000 C CNN
F 2 "" H 2250 4150 50  0001 C CNN
F 3 "" H 2250 4150 50  0001 C CNN
	1    2250 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 5600 2250 5650
Wire Wire Line
	2250 4800 2250 4900
Wire Wire Line
	2250 4200 2250 4150
$Comp
L BSPD2_V2-rescue:SML-D13DWT86A LED4
U 1 1 5B72F99E
P 2850 4800
F 0 "LED4" H 3100 4600 50  0000 L BNN
F 1 "SML-D13DWT86A" H 2850 5050 50  0000 L BNN
F 2 "LEDC1608X65N" H 3350 4950 50  0001 L BNN
F 3 "https://docs-emea.rs-online.com/webdocs/156b/0900766b8156b2c9.pdf" H 3350 4850 50  0001 L BNN
F 4 "ROHM SML-D13DWT86A, SML 608 nm Orange LED, 1608 (0603) Milky White SMD package" H 3350 4750 50  0001 L BNN "Description"
F 5 "0.65" H 3350 4650 50  0001 L BNN "Height"
F 6 "ROHM Semiconductor" H 3350 4550 50  0001 L BNN "Manufacturer_Name"
F 7 "SML-D13DWT86A" H 3350 4450 50  0001 L BNN "Manufacturer_Part_Number"
F 8 "1332873P" H 3350 4350 50  0001 L BNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/1332873P" H 3350 4250 50  0001 L BNN "RS Price/Stock"
F 10 "SML-D13DWT86A" H 3350 4150 50  0001 L BNN "Arrow Part Number"
F 11 "http://uk.rs-online.com/web/p/products/1332873P" H 3350 4050 50  0001 L BNN "Arrow Price/Stock"
	1    2850 4800
	0    -1   -1   0   
$EndComp
$Comp
L BSPD2_V2-rescue:RC0603JR-0768RL R4
U 1 1 5B72F9AC
P 2850 4900
F 0 "R4" H 3150 5000 50  0000 L CNN
F 1 "RC0603JR-0768RL" H 2850 4700 50  0000 L CNN
F 2 "RESC1608X55N" H 3400 4950 50  0001 L CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC0603_51_RoHS_L_v5.pdf" H 3400 4850 50  0001 L CNN
F 4 "RESISTOR, 0603 68 Ohms 5% 1/10 W" H 3400 4750 50  0001 L CNN "Description"
F 5 "0.55" H 3400 4650 50  0001 L CNN "Height"
F 6 "YAGEO (PHYCOMP)" H 3400 4550 50  0001 L CNN "Manufacturer_Name"
F 7 "RC0603JR-0768RL" H 3400 4450 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 3400 4350 50  0001 L CNN "RS Part Number"
F 9 "" H 3400 4250 50  0001 L CNN "RS Price/Stock"
F 10 "RC0603JR-0768RL" H 3400 4150 50  0001 L CNN "Arrow Part Number"
F 11 "" H 3400 4050 50  0001 L CNN "Arrow Price/Stock"
	1    2850 4900
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR8
U 1 1 5B72F9B2
P 2850 4150
F 0 "#PWR8" H 2850 4000 50  0001 C CNN
F 1 "+3.3V" H 2850 4290 50  0000 C CNN
F 2 "" H 2850 4150 50  0001 C CNN
F 3 "" H 2850 4150 50  0001 C CNN
	1    2850 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 5600 2850 5650
Wire Wire Line
	2850 4800 2850 4900
Wire Wire Line
	2850 4200 2850 4150
Text GLabel 2250 5650 3    51   Input ~ 0
Output(BSPD1)
Text GLabel 2850 5650 3    51   Input ~ 0
Output(BSPD2)
Text GLabel 8100 2950 0    51   Input ~ 0
Output(BSPD1)
Text GLabel 8100 3050 0    51   Input ~ 0
Output(SA(inverted))
$Comp
L power:GND #PWR20
U 1 1 5B72FC91
P 8500 4000
F 0 "#PWR20" H 8500 3750 50  0001 C CNN
F 1 "GND" H 8500 3850 50  0000 C CNN
F 2 "" H 8500 4000 50  0001 C CNN
F 3 "" H 8500 4000 50  0001 C CNN
	1    8500 4000
	1    0    0    -1  
$EndComp
$Comp
L BSPD2_V2-rescue:RC0603JR-0710KL R9
U 1 1 5B72FC9F
P 8500 3900
F 0 "R9" H 8850 4000 50  0000 C CNN
F 1 "RC0603JR-0710KL" H 8850 3800 50  0000 C CNN
F 2 "RESC1608X55N" H 8850 3700 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC0603_51_RoHS_L_v5.pdf" H 8850 3600 50  0001 C CNN
F 4 "YAGEO (PHYCOMP) - RC0603JR-0710KL - RES, THICK FILM, 10K, 5%, 0.1W, 0603" H 8850 3500 50  0001 C CNN "Description"
F 5 "RS" H 8850 3400 50  0001 C CNN "Supplier_Name"
F 6 "" H 8850 3300 50  0001 C CNN "RS Part Number"
F 7 "YAGEO (PHYCOMP)" H 8850 3200 50  0001 C CNN "Manufacturer_Name"
F 8 "RC0603JR-0710KL" H 8850 3100 50  0001 C CNN "Manufacturer_Part_Number"
F 9 "" H 8850 3000 50  0001 C CNN "Allied_Number"
F 10 "" H 8850 2900 50  0001 C CNN "Other Part Number"
F 11 "0.55" H 8850 2800 50  0001 C CNN "Height"
	1    8500 3900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8500 3900 8500 4000
$Comp
L power:GND #PWR19
U 1 1 5B72FCF1
P 8200 3900
F 0 "#PWR19" H 8200 3650 50  0001 C CNN
F 1 "GND" H 8200 3750 50  0000 C CNN
F 2 "" H 8200 3900 50  0001 C CNN
F 3 "" H 8200 3900 50  0001 C CNN
	1    8200 3900
	1    0    0    -1  
$EndComp
$Comp
L BSPD2_V2-rescue:RC0603JR-0710KL R8
U 1 1 5B72FCFF
P 8200 3800
F 0 "R8" H 8550 3900 50  0000 C CNN
F 1 "RC0603JR-0710KL" H 8550 3700 50  0000 C CNN
F 2 "RESC1608X55N" H 8550 3600 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC0603_51_RoHS_L_v5.pdf" H 8550 3500 50  0001 C CNN
F 4 "YAGEO (PHYCOMP) - RC0603JR-0710KL - RES, THICK FILM, 10K, 5%, 0.1W, 0603" H 8550 3400 50  0001 C CNN "Description"
F 5 "RS" H 8550 3300 50  0001 C CNN "Supplier_Name"
F 6 "" H 8550 3200 50  0001 C CNN "RS Part Number"
F 7 "YAGEO (PHYCOMP)" H 8550 3100 50  0001 C CNN "Manufacturer_Name"
F 8 "RC0603JR-0710KL" H 8550 3000 50  0001 C CNN "Manufacturer_Part_Number"
F 9 "" H 8550 2900 50  0001 C CNN "Allied_Number"
F 10 "" H 8550 2800 50  0001 C CNN "Other Part Number"
F 11 "0.55" H 8550 2700 50  0001 C CNN "Height"
	1    8200 3800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8200 3800 8200 3900
Wire Wire Line
	8100 2950 8200 2950
Wire Wire Line
	8100 3050 8500 3050
Wire Wire Line
	8200 3150 8200 2950
Connection ~ 8200 2950
Wire Wire Line
	8500 3250 8500 3050
Connection ~ 8500 3050
Text GLabel 8750 1750 0    51   Input ~ 0
Sensorvoltage
$Comp
L power:+24V #PWR21
U 1 1 5B730617
P 8750 1350
F 0 "#PWR21" H 8750 1200 50  0001 C CNN
F 1 "+24V" H 8750 1490 50  0000 C CNN
F 2 "" H 8750 1350 50  0001 C CNN
F 3 "" H 8750 1350 50  0001 C CNN
	1    8750 1350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR22
U 1 1 5B730670
P 8750 1850
F 0 "#PWR22" H 8750 1600 50  0001 C CNN
F 1 "GND" H 8750 1700 50  0000 C CNN
F 2 "" H 8750 1850 50  0001 C CNN
F 3 "" H 8750 1850 50  0001 C CNN
	1    8750 1850
	1    0    0    -1  
$EndComp
Text Notes 7050 6500 0    118  ~ 0
Sensoractive is low, when brake is actuated.
Text Notes 8450 4500 0    118  ~ 0
Output(BSPD2)
Wire Notes Line
	6950 4250 11050 4250
Wire Notes Line
	6950 2200 11100 2200
Text Notes 8550 2550 0    118  ~ 0
AND Gate
Text GLabel 1550 5650 3    51   Input ~ 0
Sensoractive(inverted)
Wire Wire Line
	1000 2000 1000 2050
Wire Wire Line
	1000 1400 1000 1450
Wire Wire Line
	3350 1550 3350 1600
Wire Wire Line
	3550 1650 3550 1850
Wire Wire Line
	3350 2150 3350 2200
Wire Wire Line
	5000 2450 5000 2500
Wire Wire Line
	5000 1750 5000 1800
Wire Wire Line
	3900 3800 4100 3800
Wire Wire Line
	4100 4150 4100 4200
Wire Wire Line
	5050 6400 5150 6400
Wire Wire Line
	9150 5100 9250 5100
Wire Wire Line
	10000 3050 10350 3050
Wire Wire Line
	8200 2950 8750 2950
Wire Wire Line
	8500 3050 8750 3050
Wire Notes Line
	600  2900 6950 2900
Text GLabel 6150 4450 2    51   Input ~ 0
Output(BSPD2)
$Comp
L C1210C107M4PAC7800:C1210C107M4PAC7800 C?
U 1 1 5BA89A5A
P 5600 4500
F 0 "C?" V 5804 4628 50  0000 L CNN
F 1 "C1210C107M4PAC7800" V 5895 4628 50  0000 L CNN
F 2 "CAPC3225X280N" H 5950 4550 50  0001 L CNN
F 3 "https://content.kemet.com/datasheets/KEM_C1006_X5R_SMD.pdf" H 5950 4450 50  0001 L CNN
F 4 "Cap Ceramic 100uF 16V X5R 20% Pad SMD 1210 85C T/R" H 5950 4350 50  0001 L CNN "Description"
F 5 "2.8" H 5950 4250 50  0001 L CNN "Height"
F 6 "Kemet" H 5950 4150 50  0001 L CNN "Manufacturer_Name"
F 7 "C1210C107M4PAC7800" H 5950 4050 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 5950 3950 50  0001 L CNN "RS Part Number"
F 9 "" H 5950 3850 50  0001 L CNN "RS Price/Stock"
F 10 "C1210C107M4PAC7800" H 5950 3750 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/c1210c107m4pac7800/kemet-corporation" H 5950 3650 50  0001 L CNN "Arrow Price/Stock"
	1    5600 4500
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5BA89BF6
P 5850 4450
F 0 "R?" V 5930 4450 50  0000 C CNN
F 1 "R" V 5850 4450 50  0000 C CNN
F 2 "" V 5780 4450 50  0001 C CNN
F 3 "" H 5850 4450 50  0001 C CNN
	1    5850 4450
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5BA89CE4
P 5400 4650
F 0 "R?" V 5480 4650 50  0000 C CNN
F 1 "10k" V 5400 4650 50  0000 C CNN
F 2 "" V 5330 4650 50  0001 C CNN
F 3 "" H 5400 4650 50  0001 C CNN
	1    5400 4650
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5BA89D77
P 5500 5050
F 0 "#PWR?" H 5500 4800 50  0001 C CNN
F 1 "GND" H 5500 4900 50  0000 C CNN
F 2 "" H 5500 5050 50  0001 C CNN
F 3 "" H 5500 5050 50  0001 C CNN
	1    5500 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4500 5600 4450
Connection ~ 5600 4450
Wire Wire Line
	5600 4450 5700 4450
Wire Wire Line
	5400 4500 5400 4450
Wire Wire Line
	5250 4450 5400 4450
Connection ~ 5400 4450
Wire Wire Line
	5400 4450 5600 4450
Wire Wire Line
	5500 5050 5500 5000
Wire Wire Line
	5500 5000 5600 5000
Wire Wire Line
	5500 5000 5400 5000
Wire Wire Line
	5400 5000 5400 4800
Connection ~ 5500 5000
Wire Wire Line
	6150 4450 6000 4450
Text GLabel 5850 3700 2    60   Input ~ 0
Output(Turn-onDelay)
Wire Wire Line
	5250 3300 5250 3350
Connection ~ 5250 3350
Wire Wire Line
	5250 3350 5250 3600
Wire Wire Line
	5850 3700 5200 3700
Wire Wire Line
	5250 4450 5250 3900
Wire Wire Line
	5250 3900 5200 3900
$Comp
L Device:R R?
U 1 1 5BAE8D6F
P 6300 3950
F 0 "R?" V 6380 3950 50  0000 C CNN
F 1 "R" V 6300 3950 50  0000 C CNN
F 2 "" V 6230 3950 50  0001 C CNN
F 3 "" H 6300 3950 50  0001 C CNN
	1    6300 3950
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5BAE8E63
P 5850 3950
F 0 "R?" V 5930 3950 50  0000 C CNN
F 1 "R" V 5850 3950 50  0000 C CNN
F 2 "" V 5780 3950 50  0001 C CNN
F 3 "" H 5850 3950 50  0001 C CNN
	1    5850 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6000 3950 6100 3950
$Comp
L power:+5V #PWR?
U 1 1 5BAF1BBD
P 5700 3900
F 0 "#PWR?" H 5700 3750 50  0001 C CNN
F 1 "+5V" H 5700 4040 50  0000 C CNN
F 2 "" H 5700 3900 50  0001 C CNN
F 3 "" H 5700 3900 50  0001 C CNN
	1    5700 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5BAF1C06
P 6450 4000
F 0 "#PWR?" H 6450 3750 50  0001 C CNN
F 1 "GND" H 6450 3850 50  0000 C CNN
F 2 "" H 6450 4000 50  0001 C CNN
F 3 "" H 6450 4000 50  0001 C CNN
	1    6450 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3900 5700 3950
Wire Wire Line
	6450 3950 6450 4000
Wire Wire Line
	6100 3950 6100 4050
Wire Wire Line
	6100 4050 5500 4050
Wire Wire Line
	5500 4050 5500 3800
Wire Wire Line
	5500 3800 5200 3800
Connection ~ 6100 3950
Wire Wire Line
	6100 3950 6150 3950
Text Notes 5600 4150 0    60   ~ 0
Pot for threshold
Text Notes 5700 4600 0    60   ~ 0
Pot for charging time
Wire Wire Line
	5250 3350 5450 3350
$EndSCHEMATC
